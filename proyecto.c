//Joel Santana___Manuel Barrera
/*El individuo debe ver las carreras con sus respectivas ponderaciones dentro de las facultades para luego poder ver todos los datos de una carrera
escogida por nombre o codigo y ya al haber seleccionado la carrera esta se guarda para poder realizar la ponderacion y ver la simulacion de la esta
El programa lleva a cabo la simulacion con la carrera escogida en la opcion 2.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int posc;

typedef struct{//se define la estructura 
	char nombre[84];
	int codigo,cpsu,cbea;
	float nem,rank,leng,mat,hist,cs,pond,psu,max,min;
}carrera;

void mostrar(int b,carrera car[]){//funcion que carga las carreras de una facultad escogida
	int i = 0,c;
	FILE *file;
	if(b==1)//b es el numero que representa una facultad y para abrir el archivo correcto se compara con cada numero con el que la facultad se identifica
		file=fopen("arqui.txt","r");
	else if(b==2)
		file=fopen("facultad_ciencias.txt","r");
	else if(b==3)
		file=fopen("facultad_ciencias_del_mar_y_de_recursos_naturales.txt","r");
	else if(b==4)
		file=fopen("facultad_ciencias_econimicas_y_administrativas.txt","r");
	else if(b==5)
		file=fopen("facultad_derecho.txt","r");
	else if(b==6)
		file=fopen("facultad_de_farmacias.txt","r");
	else if(b==7)
		file=fopen("facultad_humanidades.txt","r");
	else if(b==8)
		file=fopen("facultad_ingenieria.txt","r");
	else if(b==9)
		file=fopen("facultad_medicina.txt","r");
	else if(b==10)
		file=fopen("facultad_odontologia.txt","r");
	if(file==NULL){//si ek archivo no se puede abrir el programa se cierra
		printf("\nError al abrir el archivo\n");
		exit(0); 
	}
	else{//de lo contrario se lee el archivo seleccionado y se carga en la estructura y se muestra en pantalla las ponderaciones
		printf("\nCarreras en la facultad: \n");
		while (feof(file) == 0) {
			fgets(car[i].nombre,83,file);//toma el nombre de la carrera
			if(fgets(car[i].nombre,83,file)!="\n"){
				fscanf(file,"%i %f %f %f %f %f %f %f %f %f %f %i %i", &car[i].codigo, &car[i].nem, &car[i].rank, &car[i].leng, &car[i].mat, &car[i].hist, &car[i].cs, &car[i].pond, &car[i].psu, &car[i].max, &car[i].min,&car[i].cpsu,&car[i].cbea);
            	printf("%sCÓDIGO\tNEM\tRANK\tLENG\tMAT\tHIST\tCS\n%i\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\n",car[i].nombre,car[i].codigo,car[i].nem,car[i].rank,car[i].leng,car[i].mat,car[i].hist,car[i].cs);
				i++;
			}else
				fgets(car[i].nombre,83,file);
		}
		fclose(file);
	}
}

void ingreso(carrera car[83]){
	char nombre[10];
	float nem,rank,leng,mat,hist,cs,total,final;
	int i;
	char name[90];
	int ret,pos;
	//se pide el ingreso de datos para poder hacer la simulacion
	printf("Ingrese su nombre:");scanf("%s",nombre);
	printf("Ingrese NEM:");scanf("%f",&nem);
	printf("Ingrese ranking:");scanf("%f",&rank);
	printf("Ingrese puntaje lenguaje:");scanf("%f",&leng);
	printf("Ingrese puntaje matematica:");scanf("%f",&mat);
	printf("Ingrese puntaje historia:");scanf("%f",&hist);
	printf("Ingrese puntaje ciencias:");scanf("%f",&cs);
	//se hace la ponderacion con los datos de la carrera
	total=(nem*car[posc].nem)/100+(rank*car[posc].rank)/100+(leng*car[posc].leng)/100+(mat*car[posc].mat)/100+(hist*car[posc].hist)/100+(cs*car[posc].cs)/100;
    if(total<car[posc].min){
        final=car[posc].min-total;
        printf("\nla diferencia con el ultimo ingresado:%.2f",final);
    }
    else{
		final=car[posc].max-total;
		printf("\nla diferencia con el primer ingresado:%.2f",final);
	}
    printf("\nSu puntaje simulado:%.2f\n",total);
}
//funcion que busca el nombre y muestra los datos en pantalla
void buscarrnom(carrera car[]){
	char name[90];
	int i,ret,pos;
	printf("Ingrese el nombre de la carrera:\n>>");//se pide ingresar el nombre
	scanf("%s",name);
	for(i=0;i<13;i++){//se recorre hasta el numero 13 ya que la facultad con mas carreras tiene 13 carreras
		ret=strncmp(car[i].nombre,name,4);//con strncmp se espera que ret sea 0 en los primeros 4(el nombre mas corto de las carreras) caracteres se evita violacion de segmentacion
		if(ret==0){
			pos=i;
			posc=i;
			break;
		}
	}
	printf("%sCÓDIGO\tNEM\tRANK\tLENG\tMAT\tHIST\tCS\tPOND\tPSU\tMAX\tMIN\tPSU\tBEA\n%i\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.2f\t%.2f\t%i\t%i\n",car[pos].nombre,car[pos].codigo,car[pos].nem,car[pos].rank,car[pos].leng,car[pos].mat,car[pos].hist,car[pos].cs,car[pos].pond,car[pos].psu,car[pos].max,car[pos].min,car[pos].cpsu,car[pos].cbea);
}

void buscarrcod(carrera car[]){//funcion que busca la carrera por codigo y muestra los datos en pantalla
	int code;
	int i,pos;
	printf("Ingrese el codigo de la carrera:");
	scanf("%i",&code);
	for(i=0;i<13;i++){//se compara el codigo ingresado con los codigos guardados en la estructura
		if(code==car[i].codigo){
			pos=i;
			posc=i;
			break;
		}
	}
	printf("%sCÓDIGO\tNEM\tRANK\tLENG\tMAT\tHIST\tCS\tPOND\tPSU\tMAX\tMIN\tPSU\tBEA\n%i\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.2f\t%.2f\t%i\t%i\n",car[pos].nombre,car[pos].codigo,car[pos].nem,car[pos].rank,car[pos].leng,car[pos].mat,car[pos].hist,car[pos].cs,car[pos].pond,car[pos].psu,car[pos].max,car[pos].min,car[pos].cpsu,car[pos].cbea);
}

void menu(carrera car[]){
	int op,fa,opc,cod;
	char nombr[83];
	do{
		printf("\n\t1.Datos facultad.\n\t2.Consultar datos de la carrera(seleccion para simulacion).\n\t3.Simular postulacion carrera.\n\t0.Salir\n\t>>");
		scanf("\t%d",&op);
		switch(op){
			case 1:
				//se muestran las facultades a escoger para obetener las carreras de cada esta
				printf("\n\t1.FACULTAD DE ARQUITECTURA");
				printf("\n\t2.FACULTAD DE CIENCIAS");
				printf("\n\t3.FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES");
				printf("\n\t4.FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS");
				printf("\n\t5.FACULTAD DE DERECHO Y CIENCIAS SOCIALES");
				printf("\n\t6.FACULTAD DE FARMACIA");
				printf("\n\t7.FACULTAD DE HUMANIDADES");
				printf("\n\t8.FACULTAD DE INGENIERÍA");
				printf("\n\t9.FACULTAD DE MEDICINA");
				printf("\n\t10.FACULTAD DE ODONTOLOGÍA\n\t0.Salir\n\t>>");
				scanf("%i",&fa);
				if(fa==0)
					break;
				while(fa<1 || fa>10){//restriccion que evita el mal ingreso
					printf("INGRESE BIEN EL VALOR");
					scanf("%i",&fa);
				}
				mostrar(fa,car);
				break;
			case 2:
				//se pregunta como buscara la carrera
				printf("como buscara la carrera por:\n1)codigo\n2)nombre\n>>");scanf("%d",&opc);
				if(opc==1){
					buscarrcod(car);
				}
				if(opc==2){
					buscarrnom(car);
				}
				break;
			case 3:
				//solo se llama a la funcion
				ingreso(car);
				break;
		}
	}while(op!=0);
}
int main(){
	printf("\n\t\t\t**Por favor ejecutar el programa en orden 1-3**\n");
	carrera car[83];
	menu(car);
	return EXIT_SUCCESS;
}